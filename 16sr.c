#include<stdio.h>
#include<locale.h>
#include<math.h>
#include<malloc.h>
#define N 1001
char *c[100];
void sort(char **);//function, which sorts array of strings
void printStr(char *);//output function
char * readStr();//function to read array of strings
int isLetter(char);
char ** readMas();
char ** readFromFile();
void printMas(char **);
int compareArr(char *, char *);
void print_intervals(char **);
int main(void){
	char ** pisos;
	pisos = readMas();
	printMas(pisos); 
	sort(pisos);
	printMas(pisos);
	print_intervals(pisos);	
	pisos = readFromFile();
	printMas(pisos);
	return 0;
}
char * readStr(){
	char * output = (char *) malloc(sizeof(char));
	int i = 0;
	char scanned = 'a';
	while(scanned != '\0'){
		scanned = getchar();
		if(isLetter(scanned)){
			*(output+i) = scanned;
			output = (char *) realloc(output, i+2);
			++i;
		}
		else{
			*(output+i) = '\0';
			break;
		}
	}
	return output;
}
int isLetter(char c){
	if(c >='a' && c <='z') return 1;
	else if(c >= 'A' && c <= 'Z')return 1;
	else return 0;
}
void printStr(char * arr){
	int i = 0;
	while(*(arr+i) != '\0'){
		putchar(*(arr+i));
		++i;
	}
}

char ** readMas(){
	char ** output;
	int i = 0;
	output = (char **) malloc(sizeof(char**));
	char * str = readStr();
	while(*(str)!= '\0'){
		*(output+i) = str;
		output = (char **) realloc(output, (i+2)*sizeof(char *));
		str = readStr();
		++i;
	}
	*(output+i) = str;
	return output;
}

char ** readFromFile(){
 char ** arr  = (char **)malloc(sizeof(char *));
        char * st = (char *)malloc(sizeof(char));
        int i = 0, j = 0;
        char ch;
        FILE * stream;
        stream = fopen("new2.txt","r");
        int check = 0;
        while ((ch = getc(stream)) != EOF){
                if(isLetter(ch)){
                        *(st+j) = ch;
                        st = (char *)realloc(st, sizeof(char)*(j+2));
                        ++j;
			check = 1;
                }
                else{
                        if(check){
                                *(st+j) = '\0';
                                *(arr+i) = st;
                                j = 0;
                                st = (char *)malloc(sizeof(char));
                                arr = (char **)realloc(arr, sizeof(char *)*(i+2));
                                ++i;
				check = 0;
                        }
                }
        }
        char * end = (char *)malloc(sizeof(char));
        *end = '\0';
	*(arr+i) = end;
        fclose(stream);
        fflush(stdin);
	return arr;
}
void printMas(char ** arr){
	int i = 0;
	while(*(*(arr+i)) !=  '\0'){
		printStr(*(arr+i));
		putchar('\n');
		++i;
	}
}
int compareArr(char * first, char * second){//returns 1 if first > than second else 0
	int i = 0;
	while(*(first+i) != '\0' && *(second+i) != '\0'){
		if(*(first+i) > *(second+i)){//comparison of characters
			return 1;
		}
		else if(*(first+i) < *(second+i)) return 0;
		++i;
	}
	if(*(first+i) != '\0') return 1;//if size of first is bigger than size of second
	return 0;
}
void sort(char ** arr){
	int i = 0, j = 0, size = 0;
	for(i = 0; *(*(arr+i)) != '\0';++i){}
	size = i;
	char * buffer;
	for( i=0; i < size; i++) {   
    		for( j = size-1; j > i; j-- ) {    
			if (compareArr(*(arr+j-1),*(arr+j))>0) {
      				buffer=*(arr+j-1); 
				*(arr+j-1)=*(arr+j);
				* (arr+j)=buffer;
    			}
  		}
	}
}
void print_intervals(char ** arr){
	int i = 0, size, interval_1, interval_2, interval_3;
	for(;*(*(arr+i)) != '\0';++i){}
	size = i;
	interval_1 = size/3;
	interval_2 = size/3;
	interval_3 = size/3;
	if(size%3 == 1)interval_2++;
	if(size%3 == 2)interval_3++;
	printf("[");
	printf("%c",*(*(arr)));
	printf("-");
	printf("%c",*(*(arr+interval_1-1)));
	printf("] ");
	printf("%d",interval_1);
	printf(" terms ");
	printf("[");
	printf("%c",*(*(arr+interval_1)));
	printf("-");
	printf("%c",*(*(arr+interval_1+interval_2-1)));
	printf("] ");
	printf("%d",interval_2);
	printf(" terms");
	printf("[");
	printf("%c",*(*(arr+interval_2+ interval_1)));
	printf("-");
	printf("%c",*(*(arr+size - 1)));
	printf("] ");
	printf("%d",interval_3);;
	printf(" terms");
}
